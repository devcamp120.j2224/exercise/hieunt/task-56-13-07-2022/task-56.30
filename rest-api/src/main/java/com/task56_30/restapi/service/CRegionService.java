package com.task56_30.restapi.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.task56_30.restapi.model.CRegion;

@Service
public class CRegionService {
    private static List<CRegion> regionListVN = new ArrayList<CRegion>();
    private static List<CRegion> regionListUSA = new ArrayList<CRegion>();
    private static List<CRegion> regionListAUS = new ArrayList<CRegion>();

    static {
        CRegion regionVN1 = new CRegion("VN1", "Ha Noi");
        CRegion regionVN2 = new CRegion("VN2", "Da Nang");
        CRegion regionVN3 = new CRegion("VN3", "Ho Chi Minh");
        CRegion regionUSA1 = new CRegion("USA1", "Washington");
        CRegion regionUSA2 = new CRegion("USA2", "New York");
        CRegion regionUSA3 = new CRegion("USA3", "California");
        CRegion regionAUS1 = new CRegion("AUS1", "Canberra");
        CRegion regionAUS2 = new CRegion("AUS2", "Sydney");
        CRegion regionAUS3 = new CRegion("AUS3", "Melbourne");

        regionListVN.add(regionVN1);
        regionListVN.add(regionVN2);
        regionListVN.add(regionVN3);
        regionListUSA.add(regionUSA1);
        regionListUSA.add(regionUSA2);
        regionListUSA.add(regionUSA3);
        regionListAUS.add(regionAUS1);
        regionListAUS.add(regionAUS2);
        regionListAUS.add(regionAUS3);
    }
    // các getter setter
    public static List<CRegion> getRegionListVN() {
        return regionListVN;
    }
    public static void setRegionListVN(List<CRegion> regionListVN) {
        CRegionService.regionListVN = regionListVN;
    }
    public static List<CRegion> getRegionListUSA() {
        return regionListUSA;
    }
    public static void setRegionListUSA(List<CRegion> regionListUSA) {
        CRegionService.regionListUSA = regionListUSA;
    }
    public static List<CRegion> getRegionListAUS() {
        return regionListAUS;
    }
    public static void setRegionListAUS(List<CRegion> regionListAUS) {
        CRegionService.regionListAUS = regionListAUS;
    }
}
