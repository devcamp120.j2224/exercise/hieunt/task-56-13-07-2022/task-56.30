package com.task56_30.restapi.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.task56_30.restapi.model.CCountry;

@Service
public class CCountryService {
    private static List<CCountry> countryList = new ArrayList<CCountry>();

    @Autowired
    static CRegionService regions;
    static {
        CCountry countryVN = new CCountry("VN", "Viet Nam", null);
        CCountry countryUSA = new CCountry("USA", "America", null);
        CCountry countryAUS = new CCountry("AUS", "Australia", null);
        countryList.add(countryVN);
        countryList.add(countryUSA);
        countryList.add(countryAUS);
        for (int i = 0; i < countryList.size(); i++) {
            if(countryList.get(i).getCountryCode() == "VN") {
                countryList.get(i).setRegions(CRegionService.getRegionListVN());
            }
            else if(countryList.get(i).getCountryCode() == "USA") {
                countryList.get(i).setRegions(CRegionService.getRegionListUSA());
            }
            else if(countryList.get(i).getCountryCode() == "AUS") {
                countryList.get(i).setRegions(CRegionService.getRegionListAUS());
            }
        }
    }
    // getter setter
    public static List<CCountry> getCountryList() {
        return countryList;
    }
    public static void setCountryList(List<CCountry> countryList) {
        CCountryService.countryList = countryList;
    }
    // khởi tạo không tham số
    public CCountryService() {
        super();
    }  
}
