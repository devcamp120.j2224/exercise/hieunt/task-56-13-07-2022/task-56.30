package com.task56_30.restapi.model;
import java.util.List;

public class CCountry {
    private String countryCode;
    private String countryName;
    private List<CRegion> regions;
    // khởi tạo không tham số
    public CCountry() { }
    // khởi tạo đủ tất cả tham số
    public CCountry(String countryCode,
    String countryName, List<CRegion> regions) {
        super();
        this.countryCode = countryCode;
        this.countryName = countryName;
        this.regions = regions;
    }
    // các getter setter
    public String getCountryCode() {
        return countryCode;
    }
    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }
    public String getCountryName() {
        return countryName;
    }
    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }
    public List<CRegion> getRegions() {
        return regions;
    }
    public void setRegions(List<CRegion> regions) {
        this.regions = regions;
    }
}
