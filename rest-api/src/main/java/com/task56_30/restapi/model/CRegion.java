package com.task56_30.restapi.model;

public class CRegion {
    private String regionCode;
    private String regionName;
    // khởi tạo không tham số
    public CRegion() { }
    // khởi tạo đủ tất cả tham số
    public CRegion(String regionCode, String regionName) {
        super();
        this.regionCode = regionCode;
        this.regionName = regionName;
    }
    // các getter setter
    public String getRegionCode() {
        return regionCode;
    }
    public void setRegionCode(String regionCode) {
        this.regionCode = regionCode;
    }
    public String getRegionName() {
        return regionName;
    }
    public void setRegionName(String regionName) {
        this.regionName = regionName;
    }
}
