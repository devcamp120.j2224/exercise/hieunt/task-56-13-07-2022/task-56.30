package com.task56_30.restapi.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.task56_30.restapi.model.*;
import com.task56_30.restapi.service.*;

@CrossOrigin(value = "*", maxAge = -1)
@RequestMapping("/")
@RestController
public class CCountryController {
    @Autowired
    static CCountryService countries;

    @GetMapping("/countries")
    public List<CCountry> getCountryList() throws Exception {
        List<CCountry> allCountries = CCountryService.getCountryList();
        return allCountries;
    }

    @GetMapping("/country")
    public Map<String, Object> getRegionListOfCountry(
        @RequestParam(required = true, name = "countryCode") String paramCountryCode)
    throws Exception {
        Map<String, Object> responseObj = new HashMap<String, Object>();
        List<CRegion> foundedRegionList = null;

        int i = 0;
        boolean isFounded = false;
        List<CCountry> countryList = CCountryService.getCountryList();
        while (isFounded == false && i < countryList.size()) {
            CCountry bCountry = countryList.get(i);
            if(bCountry.getCountryCode().equals(paramCountryCode)) {
                isFounded = true;
                foundedRegionList = bCountry.getRegions();
                responseObj.put("regions", foundedRegionList);
                responseObj.put("status", "founded!");
            }
            else {
                i ++;
            }
        }
        if(isFounded == false) { // nếu hết vòng lặp vẫn không tìm thấy
            responseObj.put("regions", null);
            responseObj.put("status", "not founded!");
        }
        return responseObj;
    }

    @PostMapping("/countries/addnew")
    public CCountry createNewCCountry(@RequestBody CCountry newCountry) {
        CCountryService.getCountryList().add(newCountry);
        System.out.println(CCountryService.getCountryList());
        return newCountry;
    }
}
